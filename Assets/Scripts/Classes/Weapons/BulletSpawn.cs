﻿using UnityEngine;

public class BulletSpawn : MonoBehaviour {

    [Tooltip("This layers will be ignored when colliding")]
    public string[] ignoredLayers;
    [Tooltip("This tags will be ignored when colliding")]
    public string[] ignoredTags;

    [HideInInspector]
    public bool isColliding = false;

    private void OnCollisionEnter(Collision collision)
    {
        if (isRelevantCollision(collision))
        {
            isColliding = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (isRelevantCollision(collision))
        {
            isColliding = false;
        }        
    }

    private bool isRelevantCollision(Collision collision)
    {
        foreach (string ignoredLayer in ignoredLayers)
        {
            if (collision.gameObject.layer == LayerMask.NameToLayer(ignoredLayer))
            {
                return false;
            }
        }
        foreach (string ignoredTag in ignoredTags)
        {
            if (collision.gameObject.tag.Equals(ignoredTag))
            {
                return false;
            }
        }
        return true;
    }
}
