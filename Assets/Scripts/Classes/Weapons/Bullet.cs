﻿using System.Collections;
using UnityEngine;

public class Bullet : MonoBehaviour, SlowmotionObject {

    [Header("Audio")]
    [Tooltip("GameObjects audio source for weapon fire sounds")]
    public AudioSource fireAudio;
    [Tooltip("The max pitch for the fire sound")]
    public float maxPitch = 1.2f;
    [Tooltip("The min pitch for the fire sound")]
    public float minPitch = 0.8f;

    [Header("Other")]
    [Tooltip("GameObjects with this name will be ignored on collisions")]
    public string[] ignoredGameObjectNames;
    [Tooltip("GameObjects with this tag will be ignored on collisions")]
    public string[] ignoredGameObjectTags;
    [Tooltip("The bullet trail effect")]
    public TrailRenderer slowMotionTrailEffect;


    short teamID;
    short slowmotionCount;
    float defaultDamage;
    bool hitSomething = false;
    bool slowmotionActive = false;
    bool getsDestroyed = false;

    private void Start()
    {
        PlaySound(fireAudio);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (getsDestroyed)
        {
            return;
        }

        foreach(string ignoredGameObjectName in ignoredGameObjectNames)
        {
            if (other.name.Equals(ignoredGameObjectName))
            {
                return;
            }
        }

        foreach (string ignoredGameObjectTag in ignoredGameObjectTags)
        {
            if (other.tag.Equals(ignoredGameObjectTag))
            {
                return;
            }
        }

        if (hitSomething)
        {
            return;
        }

        hitSomething = true;

        if (other.transform.root.tag.Equals("Player"))
        {
            Transform player = other.transform.root;

            if(player.GetComponent<PlayerInfo>().teamID == teamID ||teamID == 0)
            {
                return;
            }

            ColliderDamage colliderDamage = other.GetComponent<ColliderDamage>();
            if(colliderDamage != null)
            {
                player.GetComponent<PlayerHealth>().TakeDamage(colliderDamage.damage);
            }
        }

        if (fireAudio != null && !fireAudio.isPlaying)
        {
            Destroy(gameObject);
        }
        else
        {
            getsDestroyed = true;
            gameObject.GetComponent<Renderer>().enabled = false;
            StartCoroutine(DestroyObjectAfterSound());
        }
        
    }

    internal void initialize(short teamID)
    {
        this.teamID = teamID;
    }

    public void toggleSlowmotion(float slowmotionMultiplier, bool activate)
    {
        if (activate)
        {
            slowmotionCount++;
        }
        if(slowmotionActive && activate || !slowmotionActive && !activate)
        {
            return;
        }
        if (!slowmotionActive)
        {
            gameObject.GetComponent<Rigidbody>().velocity *= slowmotionMultiplier;
            slowMotionTrailEffect.enabled = true;
            slowmotionActive = true;
        }
        else
        {
            if (--slowmotionCount == 0)
            {
                gameObject.GetComponent<Rigidbody>().velocity *= 1 / slowmotionMultiplier;
                slowMotionTrailEffect.enabled = false;
                slowmotionActive = false;
            } 
        }     
    }

    public void PlaySound(AudioSource audio)
    {
        if (audio != null)
        {
            audio.pitch = Random.Range(minPitch, maxPitch);
            audio.Play();
        }
    }

    IEnumerator DestroyObjectAfterSound()
    {
        if(fireAudio != null)
        {
            float audioLength = fireAudio.clip.length;
            float currentPosition = fireAudio.time;

            if (audioLength - currentPosition > 0)
            {
                yield return new WaitForSeconds(audioLength - currentPosition);
            }
        }

        Destroy(gameObject);
    }

}
