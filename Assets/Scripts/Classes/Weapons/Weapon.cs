﻿using UnityEngine;

public class Weapon : MonoBehaviour {

    [Header("Weapon Properties")]
    [Tooltip("The speed of the bullet")]
    public float bulletSpeed;
    [Tooltip("The maximum time a bullet can fly")]
    public float maxShootingTime;
    [Tooltip("The number of bullets the weapon can hold")]
    public short maxBullets;

    [Header("Other")]
    [Tooltip("The fired bullet")]
    public GameObject bullet;
    [Tooltip("The spawn point of the bullets")]
    public Transform bulletSpawnPoint;

    [HideInInspector]
    public bool reloading = false;
    [HideInInspector]
    public short bullets;

    bool slowmotionActive;

    private void Start()
    {
        bullets = maxBullets;
    }

    public Transform GetSpawnPoint()
    {
        return bulletSpawnPoint;
    }
}
