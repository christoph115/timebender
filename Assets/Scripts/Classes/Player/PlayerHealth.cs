﻿using UnityEngine;
using UnityEngine.Networking;

//Sources: Unity Manual. Code is heavily altered.

public class PlayerHealth : NetworkBehaviour
{
    public const float maxHealth = 100;

    [Tooltip("The updatable canvas")]
    public UIManager uiManager;

    [HideInInspector, SyncVar(hook = "IncreaseDeaths")]
    public short deaths = 0;
    [HideInInspector, SyncVar(hook="SetSpawnPosition")]
    public Vector3 spawnPosition;
    [HideInInspector]
    public bool isRespawning = false;

    [SyncVar(hook = "UpdateUI")]
    float health = maxHealth;

    short teamID;
    LevelManager lm;
    LevelManager LM
    {
        get
        {
            if(lm == null)
            {
                lm = FindObjectOfType<LevelManager>();
            }
            return lm;
        }
    }

    public override void OnStartLocalPlayer()
    {
        uiManager.SetUI(true);
        UpdateUI(health);
    }

    public void TakeDamage(float damage)
    {        
        if (!isServer || isRespawning)
        {
            return;
        }

        health -= damage;

        if (health <= 0)
        {
            deaths++;
            health = maxHealth;
            RpcRespawn();
        }

        return;
    }

    void UpdateUI(float health)
    {
        if (!isLocalPlayer)
        {
            return;
        }
        if(!(health == maxHealth))
        {
            LM.PlayHitSound();
        }
        uiManager.UpdateUI(health, maxHealth);
    }

    void IncreaseDeaths(short deaths)
    {
        if (isLocalPlayer)
        {
            CmdUpdateLevelManager();
        }
    }

    void SetSpawnPosition(Vector3 newSpawnPosition)
    {
        spawnPosition = newSpawnPosition;
        Spawn();
    }

    [Command]
    void CmdUpdateLevelManager()
    {
        LM.IncreaseTeamDeaths(GetComponent<PlayerInfo>().teamID);
    }

    public void Spawn()
    {
        if (isLocalPlayer)
        {
            transform.position = spawnPosition;
        }
    }

    [ClientRpc]
    public void RpcRespawn()
    {
        StartCoroutine(LM.PlayerRespawn(gameObject, spawnPosition, isLocalPlayer));
    }

}
