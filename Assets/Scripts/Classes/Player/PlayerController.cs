﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

//Sources (only for the moving itself -> Move() function, rest was programmed by myself): Unity Manual, Youtube (https://www.youtube.com/watch?v=ZwD1UHNCzOc&list=PLFt_AvWsXl0djuNM22htmz3BUtHHtOh7v&index=7&t=627s)

public class PlayerController : NetworkBehaviour, SlowmotionObject
{
    [Header("Prefabs")]
    [Tooltip("The weapon of the player")]
    public Weapon weapon;
    [Tooltip("This prefab will be rotated along the x-axis")]
    public GameObject cameraXRotation;
    [Tooltip("The object that is spawned if the player starts slow motion")]
    public GameObject slowMotionObject;

    [Header("Speed")]
    [Tooltip("The walking speed")]
    public float walkSpeed = 2;
    [Tooltip("The running speed")]
    public float runSpeed = 6;
    [Tooltip("The speed when walking backwards")]
    public float backWalkSpeed = 2;
    [Tooltip("The speed when running backwards")]
    public float backRunSpeed = 5;
    [Tooltip("The speed when turning")]
    public float turnSpeed = 5;
    [Tooltip("The speed of up and down (camera) movement")]
    public float pitchSpeed = 10;
    [Tooltip("The reloading time")]
    public float reloadTime = 1;

    [Header("Smoothness")]
    [Tooltip("How smoothly the character will turn")]
    public float turnSmooth = 1;
    [Tooltip("How smoothly the character will accelerate")]
    public float speedSmooth = 0.05f;
    [Tooltip("The smoothness of up and down (camera) movement")]
    public float pitchSmooth = 0.05f;

    [Header("Camera Movement")]
    [Tooltip("The gameobject with the camera")]
    public Transform cameraSockel;
    [Tooltip("The minimum pitch")]
    public int pitchMin = -40;
    [Tooltip("The maximum pitch")]
    public int pitchMax = 85;

    [Header("Audio")]
    [Tooltip("The audio source of the player for walking")]
    public AudioSource audioSource;
    [Tooltip("The audio source of the player for reloading")]
    public AudioSource reloadAudioSource;
    [Tooltip("The different walking sounds that can be played")]
    public AudioClip[] walkingSounds;

    [Header("Debugging")]
    [Tooltip("Set true if the player should be able switch to dodging mode everytime")]
    public bool manualMovementMode = false;
    [Tooltip("Set true if the player should be able switch to slowmotion")]
    public bool manualTimeMode = false;

    [Header("Other")]
    [Tooltip("The jumping height")]
    public float jumpHeight = 1;
    [Tooltip("The itensity of gravity")]
    public float gravity = -12;
    [Tooltip("The tag of the spawn points")]
    public string spawnPointTag = "SpawnPoint";
    [Tooltip("The layer of the player")]
    public string playerLayer = "PlayerLayer";
    [Tooltip("The ui manager")]
    public UIManager uiManager;
    [Tooltip("How often the player animation speed is updated across the network per second")]
    public float speedUpdateRate = 1;
    [Tooltip("The animator component")]
    public Animator animator;
    [Tooltip("The character controller")]
    public CharacterController controller;
    [Tooltip("The ik controller")]
    public IKControl ikControl;

    [HideInInspector]
    public bool ignoreInput = true;

    [SyncVar]
    float networkCurrentSpeed;
    [SyncVar]
    bool networkJump = false;
    [SyncVar]
    bool networkSlowmotion = false;

    float currentSpeed;
    float turnSmoothVelocity;
    float speedSmoothVelocity;
    float velocityY;
    float yaw;
    float maxYawDecreaseSlowmotion;
    float maxYawIncreaseSlowmotion;
    float slowmotionStartYaw;
    float pitch;
    float longestClipLength = 0;
    float slowmotionMultiplier = 1;
    short slowmotionCount = 0;
    bool isJumping = false;
    bool slowmotionActive = false;
    bool slowmotionMovement = false;
    bool usedSlowmotion = false;
    Transform rotationTarget;
    Vector3 currentRotationCharacter;
    Vector3 currentRotationCamera;
    Vector3 rotationSmoothVelocityCharacter;
    Vector3 rotationSmoothVelocityCamera;

    private void Start()
    {
        if (isLocalPlayer)
        {
            int playerLayerInt = LayerMask.NameToLayer(playerLayer);
            setChildLayerRecursive(transform, playerLayerInt);
        }

        foreach (AudioClip clip in walkingSounds)
        {
            if (clip.length > longestClipLength)
            {
                longestClipLength = clip.length;
            }
        }

        StartCoroutine(PlayWalkingSound());
    }

    public override void OnStartLocalPlayer()
    {
        Camera.main.GetComponent<CameraScript>().SetTarget(cameraSockel);

        maxYawDecreaseSlowmotion = Mathf.Abs(ikControl.rightHandIkTargetRotationMax);
        maxYawIncreaseSlowmotion = Mathf.Abs(ikControl.rightHandIkTargetRotationMin);

        uiManager.refreshBullets(weapon.maxBullets);

        StartCoroutine(UpdateNetworkWalkingSpeed());
    }

    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        if (ACEHelper.instance.presentationMode)
        {
            if (Input.GetButtonDown("NextSlide"))
            {
                Debug.Log("yeah");
                PresentationManager.instance.NextSlide();
            }
            if (Input.GetButtonDown("PrevSlide"))
            {
                PresentationManager.instance.PrevSlide();
            }
            if (Input.GetButtonDown("OpenSlides"))
            {
                PresentationManager.instance.OpenPresentation();
            }
            if (Input.GetButtonDown("CloseSlides"))
            {
                PresentationManager.instance.ClosePresentation();
            }
        }

        if (Input.GetButtonDown("Menu"))
        {
            uiManager.toggleInGameMenu();
        }

        if (!ignoreInput)
        {
            if (Input.GetButtonDown("Fire"))
            {
                if (weapon.bullets > 0 && !weapon.reloading)
                {
                    weapon.bullets--;
                    uiManager.refreshBullets(weapon.bullets);
                    CmdFireBullet();
                    ikControl.fireWeapon = true;
                }
            }

            if (Input.GetButtonDown("Reload"))
            {
                StartCoroutine(ReloadWeapon());
            }

            if (Input.GetButtonDown("Slowmotion") && !usedSlowmotion)
            {
                CmdSpawnSlowmotion(transform.position);
                usedSlowmotion = true;
            }

            if (manualTimeMode)
            {
                if (Input.GetButtonDown("Time Button"))
                {
                    toggleSlowmotion(Settings.slowmotionMultiplier, !slowmotionActive);
                }
            }
            if (slowmotionActive || manualTimeMode)
            {
                if (Input.GetButtonDown("Dodging Mode"))
                {
                    toggleSlowmotionMovement();
                }
            }

            //turn player and camera
            yaw += Input.GetAxis("Camera Vertical") * turnSpeed;
            pitch -= Input.GetAxis("Camera Horizontal") * pitchSpeed;
            pitch = Mathf.Clamp(pitch, pitchMin, pitchMax);
        }

        if (slowmotionMovement)
        {
            yaw = Mathf.Clamp(yaw, (slowmotionStartYaw + ikControl.currentSlowmotionHipRotationDelta) - maxYawDecreaseSlowmotion, (slowmotionStartYaw + ikControl.currentSlowmotionHipRotationDelta) + maxYawIncreaseSlowmotion);
        }

        currentRotationCharacter = Vector3.Lerp(currentRotationCharacter, new Vector3(transform.eulerAngles.x, yaw), turnSmooth); //Vector3.SmoothDamp(currentRotationCharacter, new Vector3(transform.eulerAngles.x, yaw), ref rotationSmoothVelocityCharacter, turnSmooth);

        currentRotationCamera = Vector3.Lerp(currentRotationCamera, new Vector3(pitch, yaw), pitchSmooth); //Vector3.SmoothDamp(currentRotationCamera, new Vector3(pitch, yaw), ref rotationSmoothVelocityCamera, pitchSmooth);
        cameraXRotation.transform.eulerAngles = currentRotationCamera;

        /*
        if (slowmotionMovement)
        {
            Animate(0, 0);
            return;
        }
        */

        transform.eulerAngles = currentRotationCharacter;

        //get movement input
        Vector2 movementInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        if (ignoreInput)
        {
            movementInput = new Vector2(0, 0);
        }
        Vector2 MovementInputDir = movementInput.normalized;

        bool isRunning = (Input.GetButton("Run"));

        float speed;
        float animationSpeedPercent = MovementInputDir.y;
        float animationDirectionPercent = MovementInputDir.x;

        //calculate movement
        Move(MovementInputDir, isRunning);

        //jump
        if (Input.GetButtonDown("Jump"))
        {
            Jump();
        }

        if (isRunning)
        {
            if (MovementInputDir.x < 0)
            {
                speed = currentSpeed / backRunSpeed;
            }
            else
            {
                speed = currentSpeed / runSpeed;
            }
        }
        else
        {
            if (MovementInputDir.x < 0)
            {
                speed = currentSpeed / backWalkSpeed * 0.5f;
            }
            else
            {
                speed = currentSpeed / walkSpeed * 0.5f;
            }
        }

        animationSpeedPercent *= speed;
        animationDirectionPercent *= speed;

        //animate
        Animate(animationSpeedPercent, animationDirectionPercent);

    }

    void Move(Vector2 direction, bool running)
    {
        float targetSpeed = direction.magnitude;

        if (running)
        {
            if (direction.y < 0)
            {
                targetSpeed *= backRunSpeed;
            }
            else
            {
                targetSpeed *= runSpeed;
            }
        }
        else
        {
            if (direction.y < 0)
            {
                targetSpeed *= backWalkSpeed;
            }
            else
            {
                targetSpeed *= walkSpeed;
            }
        }

        currentSpeed = Mathf.SmoothDamp(currentSpeed, targetSpeed, ref speedSmoothVelocity, speedSmooth);

        velocityY += gravity * Time.deltaTime;
        Vector3 velocity = transform.forward * currentSpeed * direction.y + transform.right * currentSpeed * direction.x + Vector3.up * velocityY;
        if (controller.enabled)
        {
            controller.Move(velocity * Time.deltaTime);
        }
        currentSpeed = new Vector2(controller.velocity.x, controller.velocity.z).magnitude;

        if (controller.isGrounded)
        {
            velocityY = 0;
            isJumping = false;
        }

    }

    private void LateUpdate()
    {
        if (!isLocalPlayer)
        {
            return;
        }
    }

    void Animate(float animationSpeedPercent, float animationDirectionPercent)
    {
        animator.SetFloat("speedPercent", animationSpeedPercent, speedSmooth, Time.deltaTime);
        animator.SetFloat("directionPercent", animationDirectionPercent, speedSmooth, Time.deltaTime);
    }

    void Jump()
    {
        if (controller.isGrounded)
        {
            float jumpVelocity = Mathf.Sqrt(-2 * gravity * jumpHeight);
            velocityY = jumpVelocity;
            isJumping = true;
        }
    }

    public void setChildLayerRecursive(Transform gameObjectTransform, int layer)
    {
        gameObjectTransform.gameObject.layer = layer;
        for(int i = 0; i < gameObjectTransform.childCount; ++i)
        {
            setChildLayerRecursive(gameObjectTransform.GetChild(i), layer);
        }
        return;
    }

    public void toggleSlowmotion(float slowmotionMultiplier, bool activate)
    {
        if (activate)
        {
            ++slowmotionCount;
        }
        if (slowmotionActive && activate || !slowmotionActive && !activate)
        {
            return;
        }

        if (slowmotionActive)
        {
            if (--slowmotionCount == 0)
            {
                slowmotionMultiplier = 1 / slowmotionMultiplier;
            }
            else
            {
                return;
            }
        }

        this.slowmotionMultiplier = slowmotionMultiplier;
        walkSpeed *= slowmotionMultiplier;
        runSpeed *= slowmotionMultiplier;
        backWalkSpeed *= slowmotionMultiplier;
        backRunSpeed *= slowmotionMultiplier;
        gravity *= slowmotionMultiplier;

        turnSpeed *= slowmotionMultiplier;
        pitchSpeed *= slowmotionMultiplier;
        reloadTime *= 1 / slowmotionMultiplier;
        reloadAudioSource.pitch *= slowmotionMultiplier;

        GetComponent<Animator>().speed *= slowmotionMultiplier;

        slowmotionActive = !slowmotionActive;
        if (isLocalPlayer)
        {
            Camera.main.GetComponent<CameraScript>().EnableSlowmotionEffect(slowmotionActive);
        }

        if (!slowmotionActive && slowmotionMovement)
        {
            toggleSlowmotionMovement();
        }
    }

    public void toggleSlowmotionMovement()
    {
        if (slowmotionMovement)
        {
            //TODO: reset player position
        }
        slowmotionMovement = !slowmotionMovement;
        gameObject.GetComponent<IKControl>().SlowmotionMovement = slowmotionMovement;
        slowmotionStartYaw = yaw;
    }

    public void AbortGame()
    {
        SceneManager.LoadScene(1);
        Prototype.NetworkLobby.LobbyManager lobbyManager = FindObjectOfType<Prototype.NetworkLobby.LobbyManager>();
        if (lobbyManager != null)
        {
            lobbyManager.EndGameLoadMainPanel();
        }
    }

    IEnumerator PlayWalkingSound()
    {
        do
        {
            float speed = networkCurrentSpeed;
            bool jumping = networkJump;
            bool slowmotion = networkSlowmotion;

            if (isLocalPlayer)
            {
                speed = currentSpeed;
                jumping = !controller.isGrounded;
                slowmotion = slowmotionActive;
            }

            if (speed > 0 && !jumping)
            {
                audioSource.clip = walkingSounds[UnityEngine.Random.Range(0, walkingSounds.Length - 1)];
                float waitingSeconds = longestClipLength;

                if (!slowmotion)
                {
                    waitingSeconds *= 1.7f - speed / runSpeed;
                }
                else
                {
                    waitingSeconds *= (1.7f - speed / runSpeed) * (1 / slowmotionMultiplier); //6.3f - (speed / runSpeed) * 3;
                }

                audioSource.Play();
                yield return new WaitForSeconds(waitingSeconds);
            }
            else
            {
                yield return null;
            }
        } while (true);
    }

    
    IEnumerator UpdateNetworkWalkingSpeed()
    {
        do
        {
            CmdUpdateNetworkCurrentSpeed(currentSpeed, isJumping, slowmotionActive);
            yield return new WaitForSeconds(1 / speedUpdateRate);
        } while (true);
    }

    IEnumerator ReloadWeapon()
    {
        if (weapon.reloading)
        {
            yield return null;
        }
        else
        {
            weapon.reloading = true;
            CmdReloadWeapon();
            yield return new WaitForSeconds(reloadTime);
            weapon.reloading = false;
            weapon.bullets = weapon.maxBullets;
            uiManager.refreshBullets(weapon.bullets);
        }
    }

    [ClientRpc]
    void RpcPlayReloadSound()
    {
        reloadAudioSource.Play();
    }

    [Command]
    void CmdReloadWeapon()
    {
        reloadAudioSource.Play();
        RpcPlayReloadSound();
        StartCoroutine(ikControl.ReloadWeapon(reloadTime));
    }

    [Command]
    void CmdUpdateNetworkCurrentSpeed(float speed, bool jumping, bool slowmotion)
    {
        networkCurrentSpeed = speed;
        networkJump = jumping;
        networkSlowmotion = slowmotion;
    }

    [Command]
    private void CmdFireBullet()
    {
        GameObject firedBullet = Instantiate(weapon.bullet, weapon.bulletSpawnPoint.position, weapon.bulletSpawnPoint.rotation) as GameObject;
        firedBullet.GetComponent<Bullet>().initialize(GetComponent<PlayerInfo>().teamID);
        firedBullet.GetComponent<Rigidbody>().velocity = firedBullet.GetComponent<Rigidbody>().transform.forward * weapon.bulletSpeed;

        NetworkServer.Spawn(firedBullet);
        Destroy(firedBullet, weapon.maxShootingTime);
    }

    [Command]
    public void CmdSpawnSlowmotion(Vector3 position)
    {
        GameObject instantiatedSlowMotionObject = Instantiate(slowMotionObject, position, Quaternion.identity);

        NetworkServer.Spawn(instantiatedSlowMotionObject);
    }


}
