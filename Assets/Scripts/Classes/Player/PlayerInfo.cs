﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerInfo : NetworkBehaviour
{
    [SyncVar, HideInInspector]
    public Color playerColor;
    [SyncVar, HideInInspector]
    public string playerName;
    [SyncVar, HideInInspector]
    public short teamID;

    [SyncVar, HideInInspector]
    public bool colorChanged = false;

    private void Start()
    {
        StartCoroutine(ChangePlayerColor());
    }

    IEnumerator ChangePlayerColor()
    {
        if (!colorChanged)
        {
            yield return new WaitForSeconds(3);
        }
        else
        {
            Material[] bodyMaterials = gameObject.transform.Find("Body").GetComponent<Renderer>().materials;
            Color teamColor = Color.gray;
            switch (teamID)
            {
                case 1:
                    teamColor = Settings.teamOneColor;
                    break;
                case 2:
                    teamColor = Settings.teamTwoColor;
                    break;
            }
            bodyMaterials[0].color = teamColor;
            bodyMaterials[1].color = teamColor;
            bodyMaterials[2].color = playerColor;
        }
    }

}
