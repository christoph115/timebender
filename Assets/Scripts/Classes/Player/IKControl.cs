﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class IKControl : NetworkBehaviour
{
    [Header("Right Hand")]
    [Tooltip("The right hand itself")]
    public GameObject rightHand;
    [Tooltip("The right hand target of the IK animations")]
    public Transform rightHandIKTarget;
    [Tooltip("The maximum rotation of the right hand IK Target")]
    public float rightHandIkTargetRotationMax = 30;
    [Tooltip("The minimum rotation of the right hand IK Target")]
    public float rightHandIkTargetRotationMin = -100;

    [Header("Hip")]
    [Tooltip("The hip itself")]
    public GameObject hip;
    [Tooltip("The intensity of the hip movement")]
    public float hipMovementIntensity = 0.2f;
    [Tooltip("The maximum rotation hip on the x-axis")]
    public float hipXRotationMax = 340;
    [Tooltip("The minimum rotation hip on the x-axis")]
    public float hipXRotationMin = 200;
    [Tooltip("The maximum rotation hip on the y-axis")]
    public float hipYRotationMax = 410;
    [Tooltip("The minimum rotation hip on the y-axis")]
    public float hipYRotationMin = 310;
    [Tooltip("The intensity of the camera height adjustment when the player is in evading modus in slowmotion")]
    public float slowmotionCameraHeightAdjustmentIntensity = 0.5f;
    [Tooltip("The intensity of the camera movement on the forward axis when the player is in evading modus in slowmotion")]
    public float slowmotionCameraForwardAdjustmentIntensity = 0.2f;

    [Header("Other")]
    [Tooltip("The animator component")]
    public Animator animator;
    [Tooltip("The target of the left hand when the player reloads")]
    public Transform leftHandReloadTarget;
    [Tooltip("The camera arm transform")]
    public Transform cameraArm;

    [HideInInspector]
    public float currentSlowmotionHipRotationDelta;
    [HideInInspector]
    public bool slowmotionMovement;
    [HideInInspector]
    public bool fireWeapon = false;
    [SyncVar, HideInInspector]
    public bool reloadWeapon = false;
    [SyncVar, HideInInspector]
    float leftHandTargetPercentage = 0;

    Quaternion lastRightHandIKRotation;
    Transform hipTransform;
    Transform headTransform;
    Vector3 lastRightHandIKPosition;
    Vector3 lastHipRotation;
    Vector3 defaultCameraArmPosition;
    Vector3 rightHandIKPositionOffset = new Vector3(0, 0, 0);
    Vector3 OffsetToCompensate = new Vector3(0, 0, 0);
    Vector3 cameraToHeadOffset;
    Vector3 defaultHipRotation;
    Vector3 targetHipRotation;
    bool sentConfigCommand = false;
    bool sentResetCommand = false;
    short resetHandPosition = 0;

    public bool aceMode = false;

    [HideInInspector]
    public bool SlowmotionMovement
    {
        get
        {
            return slowmotionMovement;
        }
        set
        {
            if (slowmotionMovement)
            {
                ResetHipPosition();
                CmdResetHipPosition();
                sentConfigCommand = true;
            }
            else
            {
                ConfigResetHipPosition();
                CmdResetHipPosition();
                sentResetCommand = true;
            }

            slowmotionMovement = value;
        }
    }

    private bool isInRotationRange(float rotation, float minRotation, float maxRotation)
    {
        if(minRotation > maxRotation)
        {
            if(rotation > minRotation || rotation < maxRotation)
            {
                return true;
            }
        }
        else
        {
            if(rotation > minRotation && rotation < maxRotation)
            {
                return true;
            }
        }

        return false;
    }

    void Start()
    {
        hipTransform = animator.GetBoneTransform(HumanBodyBones.Hips);
        headTransform = animator.GetBoneTransform(HumanBodyBones.Head);

        defaultHipRotation = new Vector3(hipTransform.localEulerAngles.x, hipTransform.localEulerAngles.y + 90, hipTransform.localEulerAngles.z + 90);
        targetHipRotation = defaultHipRotation;

        defaultCameraArmPosition = cameraArm.localPosition;
    }

    void OnAnimatorIK()
    {
        if (SlowmotionMovement)
        {
            //get movement
            Vector2 movementInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            Vector2 movementInputDir = movementInput.normalized;

            float hipXRotation = hipMovementIntensity * movementInputDir.y;
            float hipYRotation = hipMovementIntensity * movementInputDir.x * -1;

            //calculate new hip position
            Vector3 newHipRotation = lastHipRotation + new Vector3(hipXRotation, hipYRotation, 0);
            newHipRotation = new Vector3(Mathf.Clamp(newHipRotation.x, hipXRotationMin, hipXRotationMax), Mathf.Clamp(newHipRotation.y, hipYRotationMin, hipYRotationMax), newHipRotation.z);

            //calculate new camera position
            float normalizedHipXRotationItensity = ((newHipRotation.x - (hipXRotationMin + (hipXRotationMax - hipXRotationMin) / 2)) * (1 / (hipXRotationMax - hipXRotationMin))) * 2;
            float normalizedHipXRotationItensityAbs = Mathf.Abs(normalizedHipXRotationItensity);

            //set camera postion
            Vector3 newCameraArmPosition = headTransform.position + cameraToHeadOffset + headTransform.forward * normalizedHipXRotationItensity * slowmotionCameraForwardAdjustmentIntensity - Vector3.up * normalizedHipXRotationItensityAbs * slowmotionCameraHeightAdjustmentIntensity;
            cameraArm.position = newCameraArmPosition;

            targetHipRotation = newHipRotation;
            CmdSetTargetHipRotation(newHipRotation, newCameraArmPosition);
        }

        //fix feet position
        Vector3 rightFootPosition = animator.GetIKPosition(AvatarIKGoal.RightFoot);
        Quaternion rightFootRotation = animator.GetIKRotation(AvatarIKGoal.RightFoot);
        animator.SetIKPositionWeight(AvatarIKGoal.RightFoot, 1);
        animator.SetIKPosition(AvatarIKGoal.RightFoot, rightFootPosition);
        animator.SetIKRotation(AvatarIKGoal.RightFoot, rightFootRotation);

        Vector3 leftFootPosition = animator.GetIKPosition(AvatarIKGoal.LeftFoot);
        Quaternion leftFootRotation = animator.GetIKRotation(AvatarIKGoal.LeftFoot);
        animator.SetIKPositionWeight(AvatarIKGoal.LeftFoot, 1);
        animator.SetIKPosition(AvatarIKGoal.LeftFoot, leftFootPosition);
        animator.SetIKRotation(AvatarIKGoal.LeftFoot, leftFootRotation);

        //set hip position
        animator.SetBoneLocalRotation(HumanBodyBones.Hips, Quaternion.Euler(targetHipRotation));

        lastHipRotation = targetHipRotation;

        if (fireWeapon)
        {
            OffsetToCompensate = new Vector3(0, 0, 0);
            rightHandIKPositionOffset = new Vector3(0, 0, 0);
            resetHandPosition = 8;

            OffsetToCompensate += Vector3.up * 0.1f;
            OffsetToCompensate /= resetHandPosition;
            rightHandIKPositionOffset += Vector3.up * 0.1f;      

            fireWeapon = false;
        }

        //set right hand position
        animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
        animator.SetIKPosition(AvatarIKGoal.RightHand, rightHandIKTarget.position + rightHandIKPositionOffset);
        animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);
        animator.SetIKRotation(AvatarIKGoal.RightHand, rightHandIKTarget.rotation);

        if (reloadWeapon)
        {
            animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, leftHandTargetPercentage);
            animator.SetIKPosition(AvatarIKGoal.LeftHand, leftHandReloadTarget.position);
            animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, leftHandTargetPercentage);
            animator.SetIKRotation(AvatarIKGoal.LeftHand, leftHandReloadTarget.rotation);
        }

        if (resetHandPosition > 0)
        {
            rightHandIKPositionOffset -= OffsetToCompensate;
            resetHandPosition--;
        }
    }

    public IEnumerator ReloadWeapon(float reloadTime)
    {
        if (reloadWeapon)
        {
            yield return null;
        }
        else
        {
            reloadWeapon = true;
            float bygoneTime = 0;
            do
            {
                bygoneTime += Time.deltaTime;
                float bygoneTimePercentageFirstHalf = bygoneTime / (reloadTime / 2);
                leftHandTargetPercentage = Mathf.Clamp(bygoneTimePercentageFirstHalf, 0, 1);
                yield return null;
            } while (bygoneTime < (reloadTime / 2));
            do
            {
                bygoneTime += Time.deltaTime;
                float bygoneTimePercentageSecondHalf = ((bygoneTime / reloadTime) - 0.5f) * 2;
                leftHandTargetPercentage = Mathf.Clamp(1 - bygoneTimePercentageSecondHalf, 0, 1);
                yield return null;
            } while (bygoneTime < reloadTime);

            reloadWeapon = false;
        }
    }

    [Command]
    void CmdSetTargetHipRotation(Vector3 newTargetHipRotation, Vector3 cameraArmPosition)
    {
        targetHipRotation = newTargetHipRotation;
        cameraArm.position = cameraArmPosition;
        RpcSetTargetHipRotation(newTargetHipRotation, cameraArmPosition);
    }

    [ClientRpc]
    void RpcSetTargetHipRotation(Vector3 newTargetHipRotation, Vector3 cameraArmPosition)
    {
        if (SlowmotionMovement)
        {
            return;
        }
        targetHipRotation = newTargetHipRotation;
        cameraArm.position = cameraArmPosition;
    }

    [Command]
    private void CmdResetHipPosition()
    {
        ResetHipPosition();
        RpcResetHipPosition();
    }

    [ClientRpc]
    void RpcResetHipPosition()
    {
        if (sentResetCommand)
        {
            sentResetCommand = false;
            return;
        }
        ResetHipPosition();
    }

    private void ResetHipPosition()
    {
        targetHipRotation = defaultHipRotation;
        cameraArm.localPosition = defaultCameraArmPosition;
    }

    [Command]
    private void CmdConfigResetHipPosition()
    {
        ConfigResetHipPosition();
        RpcConfigResetHipPosition();
    }

    [ClientRpc]
    void RpcConfigResetHipPosition()
    {
        if (sentConfigCommand)
        {
            sentConfigCommand = false;
            return;
        }
        ConfigResetHipPosition();
    }

    private void ConfigResetHipPosition()
    {
        lastHipRotation = new Vector3(hipTransform.localEulerAngles.x, hipTransform.localEulerAngles.y + 90, hipTransform.localEulerAngles.z + 90);
        
        cameraToHeadOffset = cameraArm.position - headTransform.position;
    }

}
