﻿using UnityEngine;

public class ColliderDamage : MonoBehaviour {

    [Tooltip("The damage that this collider takes")]
    public float damage = 25;

}
