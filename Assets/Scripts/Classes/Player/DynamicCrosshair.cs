﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

//Sources: Youtube (https://www.youtube.com/watch?v=rjXy8mmj-Kc). Code slightly altered

public class DynamicCrosshair : NetworkBehaviour
{

    [Tooltip("The crosshair that is rendered")]
    public GameObject dynamicCrosshair;
    [Tooltip("The tags ignored by the crosshair")]
    public List<string> ignoredTags;
    [Tooltip("Hits are calculated from this GameObject forward")]
    public GameObject bulletSpawnPoint;
    [Tooltip("The highest allowed distance to a target")]
    public float maxCrosshairDistance;
    [Tooltip("If true the crosshair will be shown at Max Crosshair Distance (if there is no nearer target)")]
    public bool showCrosshairAtMax;

    Transform mainCameraTransform;

    public override void OnStartLocalPlayer()
    {
        mainCameraTransform = Camera.main.transform;
        dynamicCrosshair.SetActive(true);
    }

    private void LateUpdate()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        else if (bulletSpawnPoint.GetComponent<BulletSpawn>().isColliding)
        {
            ToggleCrosshair(false);
        }
        else
        {
            SetCrosshairPosition(FindCrosshairPosition());
            ToggleCrosshair(true);
        }        
    }

    public void ToggleCrosshair(bool enable)
    {
        dynamicCrosshair.SetActive(enable);
    }

    Vector3 FindCrosshairPosition()
    {
        Ray aimRay = new Ray(bulletSpawnPoint.transform.position, bulletSpawnPoint.transform.forward);

        foreach (RaycastHit hit in Physics.RaycastAll(aimRay, maxCrosshairDistance))
        {
            if (!ignoredTags.Contains(hit.transform.tag))
            {
                return hit.point;
            }
        }

        if (showCrosshairAtMax)
        {
            return bulletSpawnPoint.transform.position + bulletSpawnPoint.transform.forward * maxCrosshairDistance;
        }
        return Vector3.zero;
    }

    void SetCrosshairPosition(Vector3 crosshairPosition)
    {
        dynamicCrosshair.transform.position = crosshairPosition;
        dynamicCrosshair.transform.LookAt(mainCameraTransform.transform);
    }

}
