﻿public interface SlowmotionObject
{
    void toggleSlowmotion(float slowmotionMultiplier, bool activate);
}
