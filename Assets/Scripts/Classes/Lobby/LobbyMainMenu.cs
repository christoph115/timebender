using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

namespace Prototype.NetworkLobby
{
    //Main menu, mainly only a bunch of callback called by the UI (setup throught the Inspector)
    public class LobbyMainMenu : MonoBehaviour 
    {
        public LobbyManager lobbyManager;

        public RectTransform lobbyServerList;
        public RectTransform lobbyPanel;

        public InputField ipInput;
        bool hostActive = false;

        public void OnEnable()
        {
            lobbyManager.topPanel.ToggleVisibility(true);

            ipInput.onEndEdit.RemoveAllListeners();
            ipInput.onEndEdit.AddListener(onEndEditIP);
        }

        public void OnClickHost()
        {
            if (!hostActive)
            {
                lobbyManager.StartHost();
            }
        }

        public void OnClickJoin()
        {
            lobbyManager.ChangeTo(lobbyPanel);

            lobbyManager.networkAddress = ipInput.text;
            lobbyManager.StartClient();

            lobbyManager.backDelegate = lobbyManager.StopClientClbk;
            lobbyManager.DisplayIsConnecting();

            lobbyManager.SetServerInfo("Verbinden...", lobbyManager.networkAddress);
        }

        void onEndEditIP(string text)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                OnClickJoin();
            }
        }

        public void backToMainMenu(int sceneIndex)
        {
            AsyncOperation sceneLoading = SceneManager.LoadSceneAsync(sceneIndex);
            StartCoroutine(LoadMainMenu(sceneLoading, sceneIndex));
        }

        IEnumerator LoadMainMenu(AsyncOperation sceneLoading, int sceneIndex)
        {
            do
            {
                yield return new WaitForSeconds(0.01f);
            } while (!sceneLoading.isDone);

            SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(sceneIndex));
            lobbyManager.topPanel.ToggleVisibility(false);
            gameObject.SetActive(false);
        }

    }
}
