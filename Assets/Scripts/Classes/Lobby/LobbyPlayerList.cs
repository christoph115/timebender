using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace Prototype.NetworkLobby
{
    //List of players in the lobby
    public class LobbyPlayerList : MonoBehaviour
    {
        public static LobbyPlayerList _instance = null;

        public RectTransform playerListContentTransformTeamA;
        public RectTransform playerListContentTransformTeamB;
        public GameObject warningDirectPlayServer;
        public Button switchToTeamA;
        public Button switchToTeamB;

        protected VerticalLayoutGroup _layoutTeamA;
        protected VerticalLayoutGroup _layoutTeamB;
        protected List<LobbyPlayer> _playersTeamA = new List<LobbyPlayer>();
        protected List<LobbyPlayer> _playersTeamB = new List<LobbyPlayer>();

        LobbyPlayer localPlayer;
        bool switchToTeamAActive;
        bool newPlayerSwitch;
        bool ready = false;
        public bool Ready
        {
            get { return ready; }
            set
            {
                ready = value;
                if (ready)
                {
                    switchToTeamB.interactable = false;
                    switchToTeamA.interactable = false;
                }
                else
                {
                    if(LocalPlayer == null)
                    {
                        return;
                    }
                    switch (LocalPlayer.teamID)
                    {
                        case 1:
                            SwitchActiveTeamButton(false);
                            break;
                        case 2:
                            SwitchActiveTeamButton(true);
                            break;
                    }
                }
            }
        }
        public LobbyPlayer LocalPlayer
        {
            get
            {
                if (localPlayer == null)
                {
                    foreach (LobbyPlayer player in _playersTeamA)
                    {
                        if (player.isLocalPlayer)
                        {
                            localPlayer = player;
                        }
                    }
                    if (localPlayer == null)
                    {
                        foreach (LobbyPlayer player in _playersTeamB)
                        {
                            if (player.isLocalPlayer)
                            {
                                localPlayer = player;
                            }
                        }
                    }
                }
                return localPlayer;
            }
        }

        public void OnEnable()
        {
            _instance = this;
            _layoutTeamA = playerListContentTransformTeamA.GetComponent<VerticalLayoutGroup>();
            _layoutTeamB = playerListContentTransformTeamB.GetComponent<VerticalLayoutGroup>();
        }

        private void Update()
        {
            if (newPlayerSwitch)
            {
                switch (LocalPlayer.teamID)
                {
                    case 1:
                        ChangeTeamToA(LocalPlayer);
                        break;
                    case 2:
                        ChangeTeamToB(LocalPlayer);
                        break;
                }
                newPlayerSwitch = false;
            }

            switch (LocalPlayer.teamID)
            {
                case 1:
                    SwitchActiveTeamButton(false);
                    break;
                case 2:
                    SwitchActiveTeamButton(true);
                    break;
            }
        }

        public void DisplayDirectServerWarning(bool enabled)
        {
            if(warningDirectPlayServer != null)
                warningDirectPlayServer.SetActive(enabled);
        }

        public void AddPlayer(LobbyPlayer player)
        {
            if (_playersTeamA.Contains(player) || _playersTeamB.Contains(player))
                return;

            if (player.teamID == 1)
            {
                ChangeTeamToA(player);
            }
            else if(player.teamID == 2)
            {
                ChangeTeamToB(player);
            }
            else
            {
                if (_playersTeamB.Count < _playersTeamA.Count)
                {
                    ChangeTeamToB(player);
                }
                else
                {
                    ChangeTeamToA(player);
                }
            }
        }

        public void RemovePlayer(LobbyPlayer player)
        {
            if (_playersTeamA.Contains(player))
            {
                _playersTeamA.Remove(player);
            }
            else
            {
                _playersTeamB.Remove(player);
            }
        }

        public void ChangeTeamToA(LobbyPlayer player)
        {
            if (_playersTeamB.Contains(player))
            {
                _playersTeamB.Remove(player);
            }
            if (!_playersTeamA.Contains(player))
            {
                _playersTeamA.Add(player);
            }            
            player.teamID = 1;
            player.transform.SetParent(playerListContentTransformTeamA, false);

            if (player.isLocalPlayer)
            {
                SwitchActiveTeamButton(false);
            }
            else
            {
                newPlayerSwitch = true;
            }
        }

        public void ChangeTeamToB(LobbyPlayer player)
        {
            if (_playersTeamA.Contains(player))
            {
                _playersTeamA.Remove(player);
            }
            if (!_playersTeamB.Contains(player))
            {
                _playersTeamB.Add(player);
            }
            player.teamID = 2;
            player.transform.SetParent(playerListContentTransformTeamB, false);

            if (player.isLocalPlayer)
            {
                SwitchActiveTeamButton(true);
            }
            else
            {
                newPlayerSwitch = true;
            }
        }

        public void SwitchActiveTeamButton(bool teamAButtonActive)
        {
            if (ready)
            {
                return;
            }
            if (teamAButtonActive)
            {
                if (_playersTeamB.Count > 1)
                {
                    switchToTeamA.interactable = true;
                }
                else
                {
                    switchToTeamA.interactable = false;
                }
                switchToTeamB.interactable = false;
            }
            else
            {
                if (_playersTeamA.Count > 1)
                {
                    switchToTeamB.interactable = true;
                }
                else
                {
                    switchToTeamB.interactable = false;
                }
                switchToTeamA.interactable = false;
            }
        }
    }
}
