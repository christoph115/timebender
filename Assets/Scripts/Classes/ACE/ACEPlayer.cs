﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ACEPlayer : NetworkBehaviour{

    public bool mMode = false;
    public Transform weaponTarget;
    public float degreesPerSecond;
    public float maxDegrees;
    float addedDegrees;
    public Weapon weapon;
    public float weaponFireTime = 1;

    Vector3 originalWeaponTargetRotation;
    bool addToRotation = true;

    private void Start()
    {
        originalWeaponTargetRotation = weaponTarget.rotation.eulerAngles;

        StartCoroutine(FireBulletCoroutine());
        StartCoroutine(MoveHand());
    }

    IEnumerator FireBulletCoroutine()
    {
        do{
            FireBullet();
            yield return new WaitForSeconds(weaponFireTime);
        } while(true);
    }

    IEnumerator MoveHand()
    {
        do
        {
            if (addToRotation)
            {
                float degrees = degreesPerSecond * Time.deltaTime;
                addedDegrees += degrees;
                if (addedDegrees > maxDegrees)
                {
                    degrees -= addedDegrees - maxDegrees;
                    addedDegrees = maxDegrees;
                    addToRotation = false;
                }
                weaponTarget.rotation = Quaternion.Euler(weaponTarget.rotation.eulerAngles + new Vector3(0, degrees, 0));
            }
            else
            {
                float degrees = -(degreesPerSecond * Time.deltaTime);
                addedDegrees += degrees;
                if (addedDegrees < -maxDegrees)
                {
                    degrees += Mathf.Abs(addedDegrees + maxDegrees);
                    addedDegrees = -maxDegrees;
                    addToRotation = true;
                }
                weaponTarget.rotation = Quaternion.Euler(weaponTarget.rotation.eulerAngles + new Vector3(0, degrees, 0));
            }
            yield return null;
        } while (true);

    }

    public void FireBullet()
    {
        GameObject firedBullet = Instantiate(weapon.bullet, weapon.bulletSpawnPoint.position, weapon.bulletSpawnPoint.rotation) as GameObject;
        firedBullet.GetComponent<Bullet>().initialize(2);
        firedBullet.GetComponent<Rigidbody>().velocity = firedBullet.GetComponent<Rigidbody>().transform.forward * weapon.bulletSpeed;

        NetworkServer.Spawn(firedBullet);
        Destroy(firedBullet, weapon.maxShootingTime);
    }

}
