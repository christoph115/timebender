﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PresentationManager : MonoBehaviour {

    public static PresentationManager instance;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else if(instance != this)
        {
            Destroy(gameObject);
        }
    }

    public Image displayedImage;
    public List<Sprite> slides = new List<Sprite>();
    int currentSlide = 0;

    public void NextSlide()
    {
        OpenPresentation();
        if (currentSlide + 1 < slides.Count)
        {
            currentSlide++;
            displayedImage.sprite = slides[currentSlide];
        }
        else
        {
            ClosePresentation();
        }
    }

    public void PrevSlide()
    {
        OpenPresentation();
        if (currentSlide - 1 >= 0)
        {
            currentSlide--;
            displayedImage.sprite = slides[currentSlide];
        }
        else
        {
            ClosePresentation();
        }
    }

    public void OpenPresentation()
    {
        displayedImage.gameObject.SetActive(true);
        displayedImage.sprite = slides[currentSlide];
    }

    public void ClosePresentation()
    {
        displayedImage.gameObject.SetActive(false);
    }

}
