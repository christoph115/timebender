﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ACEButton : MonoBehaviour {

    public ColorBlock activeColors;
    public ColorBlock inactiveColors;
    public Button aceButton;

    bool aceModeActive = false;

	// Use this for initialization
	void Start () {
        aceButton.colors = inactiveColors;
	}
	
    public void ButtonPressed()
    {
        aceModeActive = !aceModeActive;
        if (aceModeActive)
        {
            aceButton.colors = activeColors;
        }
        else
        {
            aceButton.colors = inactiveColors;
        }
    }

}
