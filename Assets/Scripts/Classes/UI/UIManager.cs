﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    [Tooltip("Displays the current Health")]
    public Text currentHealth;
    [Tooltip("Displays the maximum Health")]
    public Text maxHealth;
    [Tooltip("Displays the munition")]
    public Text bulletText;
    [Tooltip("The menu canvas")]
    public GameObject menuCanvas;
    [Tooltip("The player controller")]
    public PlayerController playerController;
    [Tooltip("The audio slider")]
    public Slider audioSlider;
    [Tooltip("The image that shows damage")]
    public Image damageDisplay;
    [Tooltip("The maximum itensity of the damage effect")]
    public float maxDamageEffect = 0.5f;
    [Tooltip("The threshold for the damage effect")]
    public float damageEffectThreshold = 0.5f;

    private void Start()
    {
        audioSlider.value = Settings.volume;
    }

    internal void UpdateUI(float currentHealth, float maxHealth)
    {
        this.currentHealth.text = currentHealth.ToString();
        this.maxHealth.text = maxHealth.ToString();

        float healthPercentage = currentHealth / maxHealth;

        if(healthPercentage <= damageEffectThreshold)
        {
            float alpha = (damageEffectThreshold - healthPercentage) * (1 / damageEffectThreshold);
            Color newColor = new Color(damageDisplay.color.r, damageDisplay.color.g, damageDisplay.color.b, maxDamageEffect * alpha);
            damageDisplay.color = newColor;
        }        
        else if(healthPercentage == 1)
        {
            Color newColor = new Color(damageDisplay.color.r, damageDisplay.color.g, damageDisplay.color.b, 0);
            damageDisplay.color = newColor;
        }
    }

    internal void SetUI(bool active)
    {
        gameObject.SetActive(active);
    }

    public void toggleInGameMenu()
    {
        bool menuActive = !menuCanvas.activeSelf;
        menuCanvas.SetActive(menuActive);
        Cursor.visible = menuActive;
        playerController.ignoreInput = menuActive;
    }

    public void ChangeVolume(float newVolume)
    {
        AudioListener.volume = newVolume;
        Settings.volume = newVolume;
    }

    internal void refreshBullets(short bullets)
    {
        bulletText.text = bullets.ToString();
    }
}
