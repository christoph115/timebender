﻿using UnityEngine;
using UnityEngine.UI;

public class PointStatus : MonoBehaviour {

    public Text pointText;
    public Text nameText;
    public Image background;

}
