﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class StatusUIManager : MonoBehaviour {

    [Tooltip("The prefab of the point status boxes")]
    public GameObject pointStatusPrefab;

    LevelManager levelManager;
    Dictionary<Checkpoint, PointStatus> pointStatusDictionary = new Dictionary<Checkpoint, PointStatus>();
    PointStatus killsStatus;

    private void Start()
    {
        StartCoroutine(Initialize());
    }

    IEnumerator Initialize()
    {
        do
        {
            yield return new WaitForSeconds(1);
            levelManager = FindObjectOfType<LevelManager>();
        } while (levelManager == null);

        killsStatus = Instantiate(pointStatusPrefab, gameObject.transform, false).GetComponent<PointStatus>();

        killsStatus.nameText.text = "Kills: A / B";
        killsStatus.pointText.text = GetKillsString();

        Checkpoint[] checkpoints = levelManager.Checkpoints.OrderBy(checkpoint => checkpoint.name).ToArray();

        foreach (Checkpoint checkpoint in checkpoints)
        {
            PointStatus pointStatus = Instantiate(pointStatusPrefab, gameObject.transform, false).GetComponent<PointStatus>();

            pointStatus.nameText.text = checkpoint.name;
            pointStatus.pointText.text = checkpoint.Points.ToString("0.0");

            pointStatusDictionary.Add(checkpoint, pointStatus);
        }
    }

    string GetKillsString()
    {
        if(levelManager != null)
        {
            return levelManager.teamTwoDeaths + " / " + levelManager.teamOneDeaths;
        }
        return "0 / 0";
    }

    private void Update()
    {
        if(levelManager != null)
        {
            killsStatus.pointText.text = GetKillsString();
            Color killStatusColor = GetKillstatusColor();
            killStatusColor.a = killsStatus.background.color.a;
            killsStatus.background.color = killStatusColor;

            foreach (KeyValuePair<Checkpoint, PointStatus> pointStatusEntry in pointStatusDictionary)
            {
                pointStatusEntry.Value.pointText.text = pointStatusEntry.Key.Points.ToString("0.0");
                Color checkpointColor = pointStatusEntry.Key.GetCheckpointColor();
                checkpointColor.a = pointStatusEntry.Value.background.color.a;
                pointStatusEntry.Value.background.color = checkpointColor;
            }
        }
    }

    private Color GetKillstatusColor()
    {
        Color killStatusColor = new Color(1, 1, 1);
        float pointMultiplier = 0.25f + Mathf.Abs((levelManager.teamOneDeaths - levelManager.teamTwoDeaths)) / 20f;

        if (levelManager.teamTwoDeaths > levelManager.teamOneDeaths)
        {
            killStatusColor = killStatusColor * (1 - pointMultiplier) + Settings.teamOneColor * pointMultiplier;
        }
        else if (levelManager.teamTwoDeaths < levelManager.teamOneDeaths)
        {
            killStatusColor = killStatusColor * (1 - pointMultiplier) + Settings.teamTwoColor * pointMultiplier;
        }

        return killStatusColor;
    }
}
