﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UITimer : MonoBehaviour {

    [Tooltip("The text of the timer")]
    public Text timerText;

    LevelManager levelManager;

    private void Start()
    {
        StartCoroutine(Initialize());
        timerText = GetComponent<Text>();
    }

    IEnumerator Initialize()
    {
        do
        {
            yield return new WaitForSeconds(1);
            levelManager = FindObjectOfType<LevelManager>();
        } while (levelManager == null);
    }

    void Update () {

        if(levelManager == null)
        {
            return;
        }

        timerText.text = levelManager.gameTime.ToString("0.0");
    }

}
