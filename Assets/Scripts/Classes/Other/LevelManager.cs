﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : NetworkBehaviour {

    [Header("Settings")]
    [Tooltip("The time of one match"), SyncVar]
    public float gameTime = 30;
    [Tooltip("The countdown before the match")]
    public float countdown = 3;
    [Tooltip("The time a player cannot do anything if he is dead")]
    public float deathTimeout = 3;

    [Header("UI")]
    [Tooltip("The countdown canvas")]
    public GameObject countdownCanvas;
    [Tooltip("The countdown text")]
    public Text countdownText;
    [Tooltip("The countdown background image")]
    public Image countdownImage;

    [Header("Audio")]
    [Tooltip("This audio source plays the death sound")]
    public AudioSource audioSource;
    [Tooltip("The audio clip that is played if the player dies")]
    public AudioClip deathAudio;
    [Tooltip("The audio clip that is played if the player gets hit")]
    public AudioClip hitAudio;
    [Tooltip("This audio source plays the background music")]
    public AudioSource backgroundSource;

    [HideInInspector]
    public bool gameRunning = false;
    [HideInInspector]
    public static short winner = 0;
    [HideInInspector, SyncVar]
    public short teamOneDeaths = 0;
    [HideInInspector, SyncVar]
    public short teamTwoDeaths = 0;

    public GameObject acePlayer;

    Checkpoint[] checkpoints;
    float currentCountdown;
    float startAlpha;

    public Checkpoint[] Checkpoints
    {
        get { return checkpoints; }
    }

	void Start () {
        Cursor.visible = false;

        currentCountdown = countdown;
        checkpoints = FindObjectsOfType<Checkpoint>();

        if (isServer)
        {
            StartCoroutine(TryToStartCountdown());
        }
        ACEHelper.instance.presentationMode = true;
    }

    private void Update()
    {
        if (ACEHelper.instance.presentationMode && GameObject.FindGameObjectsWithTag("Player").Length > 1)
        {
            Debug.Log(GameObject.FindGameObjectsWithTag("Player").Length);
            Destroy(acePlayer);
            ACEHelper.instance.presentationMode = false;
        }
        if (gameRunning && !ACEHelper.instance.presentationMode)
        {
            gameTime -= Time.deltaTime;
            if (gameTime <= 0)
            {
                gameTime = 0;
                gameRunning = false;
                EndGame();
            }
        }
    }

    private void EndGame()
    {
        CalcWinner();
        SceneManager.LoadScene(3);
    }

    void CalcWinner()
    {
        short teamOneWinPoints = 0;
        short teamTwoWinPoints = 0;
        float teamACheckpointPoints = 0;
        float teamBCheckpointPoints = 0;

        foreach (Checkpoint checkpoint in checkpoints)
        {
            switch (checkpoint.Owner)
            {
                case 1:
                    teamACheckpointPoints += checkpoint.Points - 50;
                    teamOneWinPoints++;
                    break;
                case 2:
                    teamBCheckpointPoints += 50 - checkpoint.Points;
                    teamTwoWinPoints++;
                    break;
            }
        }

        if (teamOneDeaths < teamTwoDeaths)
        {
            teamOneWinPoints++;
            if (teamOneWinPoints == teamTwoWinPoints)
            {
                teamOneWinPoints++;
            }
        }
        else if (teamOneDeaths > teamTwoDeaths)
        {
            teamTwoWinPoints++;
            if (teamOneWinPoints == teamTwoWinPoints)
            {
                teamTwoWinPoints++;
            }
        }

        if (teamOneWinPoints == teamTwoWinPoints)
        {
            if (teamACheckpointPoints > teamBCheckpointPoints)
            {
                teamOneWinPoints++;
            }
            else if (teamACheckpointPoints < teamBCheckpointPoints)
            {
                teamTwoWinPoints++;
            }
        }

        if (teamOneWinPoints > teamTwoWinPoints)
        {
            winner = 1;
        }
        else if (teamOneWinPoints < teamTwoWinPoints)
        {
            winner = 2;
        }
        else
        {
            winner = 3;
        }
    }

    public void IncreaseTeamDeaths(short teamID)
    {
        switch (teamID)
        {
            case 1:
                teamOneDeaths++;
                break;
            case 2:
                teamTwoDeaths++;
                break;
        }
    }

    IEnumerator TryToStartCountdown()
    {
        yield return new WaitForSeconds(10);
        CmdStartCountdown();
    }

    [Command]
    void CmdStartCountdown()
    {
        RpcStartCountdown();
    }

    [ClientRpc]
    void RpcStartCountdown()
    {
        StartCoroutine(UpdateCountdown());
    }

    IEnumerator UpdateCountdown()
    {
        startAlpha = countdownImage.color.a;
        backgroundSource.volume = 0;
        backgroundSource.Play();

        do
        {
            yield return null;
            currentCountdown -= Time.deltaTime;
            float countdownPercentage = currentCountdown / countdown;
            backgroundSource.volume = 1 - countdownPercentage;
            UpdateCountdownUI();
        } while (currentCountdown > 0);

        foreach (PlayerController playerController in FindObjectsOfType<PlayerController>())
        {
            if (playerController.isLocalPlayer)
            {
                playerController.ignoreInput = false;
                break;
            }
        }
        countdownImage.color = new Color(1, 1, 1, 0);
        countdownText.gameObject.SetActive(false);
        //ResetCountdownUI();

        backgroundSource.volume = 1;
        gameRunning = true;
    }

    public IEnumerator PlayerRespawn(GameObject player, Vector3 respawnPosition, bool localPlayer)
    {
        ResetCountdownUI();
        if (localPlayer)
        {
            if(audioSource != null && deathAudio != null)
            {
                audioSource.clip = deathAudio;
                audioSource.Play();
            }
            currentCountdown = deathTimeout;
            countdownText.text = Mathf.Ceil(currentCountdown).ToString("0");
            countdownCanvas.SetActive(true);
        }

        player.transform.position = respawnPosition;

        Renderer[] playerRenderer = player.GetComponentsInChildren<Renderer>();
        foreach(Renderer singlePlayerRenderer in playerRenderer)
        {
            singlePlayerRenderer.enabled = false;
        }
        /*
        Collider[] playerCollider = player.GetComponentsInChildren<Collider>();
        foreach (Collider SinglePlayerCollider in playerCollider)
        {
            SinglePlayerCollider.enabled = false;
        }
        */
        player.GetComponent<PlayerHealth>().isRespawning = true;

        if (localPlayer)
        {
            do
            {
                countdownText.text = Mathf.Ceil(currentCountdown).ToString("0");
                yield return null;
                currentCountdown -= Time.deltaTime;
            } while (currentCountdown >= 0);
        }
        else
        {
            yield return new WaitForSeconds(deathTimeout);
        }        

        foreach (Renderer singlePlayerRenderer in playerRenderer)
        {
            singlePlayerRenderer.enabled = true;
        }
        player.GetComponent<PlayerHealth>().isRespawning = false;
        /*
        foreach (Collider SinglePlayerCollider in playerCollider)
        {
            SinglePlayerCollider.enabled = true;
        }
        */
        if (localPlayer)
        {
            countdownImage.color = new Color(1, 1, 1, 0);
            countdownText.gameObject.SetActive(false);
            //countdownCanvas.SetActive(false);
        }

        player.GetComponentInChildren<Renderer>().enabled = true;
    }

    private void UpdateCountdownUI()
    {
        countdownText.text = Mathf.Ceil(currentCountdown).ToString("0");

        Color currentBackgroundColor = countdownImage.color;
        float newAlpha = startAlpha * (currentCountdown / countdown);
        newAlpha = Mathf.Max(newAlpha, 0);
        Color newBackgroundColor = new Color(currentBackgroundColor.r, currentBackgroundColor.g, currentBackgroundColor.b, newAlpha);
        countdownImage.color = newBackgroundColor;
    }

    private void ResetCountdownUI()
    {
        Color currentBackgroundColor = countdownImage.color;
        Color newBackgroundColor = new Color(currentBackgroundColor.r, currentBackgroundColor.g, currentBackgroundColor.b, startAlpha);
        countdownImage.color = newBackgroundColor;
        countdownText.gameObject.SetActive(true);
    }

    public void PlayHitSound()
    {
        if (audioSource != null && hitAudio != null)
        {
            audioSource.clip = hitAudio;
            audioSource.Play();
        }
    }

}
