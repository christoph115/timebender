﻿using System;
using System.Collections;
using UnityEngine;

//Sources: Unity Standard Assets, Unity Manual. Code was slightly altered.

public class Helper : MonoBehaviour {

    [Tooltip("Show the ray")]
    public bool visualizeRays = true;

    [HideInInspector]
    public enum MovementAxis { x, y, z }

    RayHitComparer rayHitComparer = new RayHitComparer();


    public float GetTargetDistance(Ray ray, float sphereCastRadius, string dontClipLayer, float maxDistance, Transform origin, float minDistanceToObsticle, MovementAxis movementAxis, bool reverseAxis)
    {
        RaycastHit[] hits;

        float targetDistance = maxDistance;

        var cols = Physics.OverlapSphere(ray.origin, sphereCastRadius);

        bool initialIntersect = false;

        for (int i = 0; i < cols.Length; i++)
        {
            if ((!cols[i].isTrigger) && !(cols[i] != null && cols[i].gameObject.layer == LayerMask.NameToLayer(dontClipLayer)))
            {
                initialIntersect = true;
                break;
            }
        }

        if (initialIntersect)
        {
            if (reverseAxis)
            {
                ray.origin -= ray.direction * sphereCastRadius;
            }
            ray.origin += ray.direction * sphereCastRadius;

            hits = Physics.RaycastAll(ray, maxDistance - sphereCastRadius);
        }
        else
        {
            hits = Physics.SphereCastAll(ray, sphereCastRadius, maxDistance + sphereCastRadius);
        }

        Array.Sort(hits, rayHitComparer);

        float nearest = Mathf.Infinity;

        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].distance < nearest && (!hits[i].collider.isTrigger) && !(hits[i].collider != null && hits[i].collider.gameObject.layer == LayerMask.NameToLayer(dontClipLayer)))
            {
                nearest = hits[i].distance;
                switch (movementAxis)
                {
                    case MovementAxis.x:
                        targetDistance = origin.InverseTransformPoint(hits[i].point).x;
                        break;
                    case MovementAxis.y:
                        targetDistance = origin.InverseTransformPoint(hits[i].point).y;
                        break;
                    case MovementAxis.z:
                        targetDistance = origin.InverseTransformPoint(hits[i].point).z;
                        break;
                }

                if (reverseAxis)
                {
                    targetDistance += (minDistanceToObsticle - nearest);
                    targetDistance *= -1;
                }
                else
                {
                    targetDistance -= (minDistanceToObsticle - nearest);
                }
            }
        }        

        Debug.DrawRay(ray.origin, ray.direction * (targetDistance + sphereCastRadius), Color.red);

        return targetDistance;
    }

    public class RayHitComparer : IComparer
    {
        public int Compare(object x, object y)
        {
            return ((RaycastHit)x).distance.CompareTo(((RaycastHit)y).distance);
        }
    }


}
