﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadLobbyScene : MonoBehaviour {

    public void LoadScene(int index)
    {
        Prototype.NetworkLobby.LobbyManager lobbyManager = FindObjectOfType<Prototype.NetworkLobby.LobbyManager>();

        if (lobbyManager != null)
        {
            lobbyManager.ChangeTo(lobbyManager.mainMenuPanel);
        }

        SceneManager.LoadScene(index);
    }

}