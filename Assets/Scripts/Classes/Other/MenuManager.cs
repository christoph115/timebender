﻿using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

    [Tooltip("The menu audio slider")]
    public Slider AudioSlider;

    private void Start()
    {
        AudioSlider.value = Settings.volume;
    }

    public void ChangeVolume(float newVolume)
    {
        AudioListener.volume = newVolume;
        Settings.volume = newVolume;
    }

}
