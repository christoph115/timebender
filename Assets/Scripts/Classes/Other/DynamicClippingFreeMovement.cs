﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

//Sources: Unity Standard Assets, Unity Manual. Code heavily altered.

public class DynamicClippingFreeMovement : MonoBehaviour
{
    [Header("Target Object")]
    public Transform targetTransform;

    [Header("Axis")]
    [Tooltip("The axis in which the object moves")]
    public Helper.MovementAxis movementAxis;
    [Tooltip("Specifies the direction in which the object moves")]
    public bool reverseMovementAxis;

    [Header("Reaction Speed")]
    [Tooltip("Time taken to move when avoiding cliping(low value = fast, which it should be)")]
    public float clipMoveTime = 0.05f;
    [Tooltip("Time taken to move back towards desired position, when not clipping (typically should be a higher value than Clip Move Time)")]
    public float returnTime = 0.4f; 

    [Header("Distances & Ignored Layer")]
    [Tooltip("The radius of the sphere used to test for object between object and target")]
    public float sphereCastRadius = 0.1f;
    [Tooltip("The closest distance the object can be from the target")]
    public float closestDistance = 0.5f;
    [Tooltip("The minimum distance between the object and the obsticle")]
    public float minDistanceToObsticle = 0;
    [Tooltip("Don't clip against objects in this Layer (useful for not clipping against the targeted object)")]
    public string dontClipLayer = "PlayerLayer";

    [Header("Offset")]
    [Tooltip("Adds an offset after all calculations (it will be ignored when calculating the rays)")]
    public Vector3 afterOffset;
    [Tooltip("Adds an offset once before all calculations")]
    public Vector3 beforeOffset;

    [Header("Other")]
    [Tooltip("Should be true if this component isn't initialized in an other script")]
    public bool autostart = false;

    private float originalDistance;
    private float moveVelocity;
    private float currentDistance;
    private Ray ray = new Ray();
    private Helper helper;
    private bool initialized = false;    

    private void Start()
    {
        if (autostart)
        {
            Initialize();
        }
    }

    public void Initialize()
    {
        if (targetTransform == null || initialized)
        {
            return;
        }

        transform.position = targetTransform.position + beforeOffset;

        switch (movementAxis)
        {
            case Helper.MovementAxis.x:
                originalDistance = Mathf.Abs((targetTransform.position - transform.position).x);
                break;
            case Helper.MovementAxis.y:
                originalDistance = Mathf.Abs((targetTransform.position - transform.position).y);
                break;
            case Helper.MovementAxis.z:
                originalDistance = Mathf.Abs((targetTransform.position - transform.position).z);
                break;
        }

        currentDistance = originalDistance;
        helper = FindObjectOfType<Helper>();
        initialized = true;
    }

    private void LateUpdate()
    {
        if((helper = FindObjectOfType<Helper>()) == null)
        {
            return;
        }

        if (targetTransform == null)
        {
            return;
        }
        Vector3 rayOriginMultiplicator = Vector3.one;
        switch (movementAxis)
        {
            case Helper.MovementAxis.x:
                ray.origin = targetTransform.position;
                rayOriginMultiplicator = -targetTransform.right;
                ray.direction = targetTransform.right;
                break;
            case Helper.MovementAxis.y:
                ray.origin = targetTransform.position;
                rayOriginMultiplicator = -targetTransform.up;
                ray.direction = targetTransform.up;
                break;
            case Helper.MovementAxis.z:
                ray.origin = targetTransform.position;
                rayOriginMultiplicator = -targetTransform.forward;
                ray.direction = targetTransform.forward;
                break;
        }
        if (reverseMovementAxis)
        {
            ray.direction *= -1;
            rayOriginMultiplicator *= -1;
        }

        ray.origin += rayOriginMultiplicator * sphereCastRadius;

        float targetDistance = helper.GetTargetDistance(ray, sphereCastRadius, dontClipLayer, originalDistance, targetTransform, minDistanceToObsticle, movementAxis, reverseMovementAxis);

        currentDistance = Mathf.SmoothDamp(currentDistance, targetDistance, ref moveVelocity, currentDistance > targetDistance ? clipMoveTime : returnTime);
        currentDistance = Mathf.Clamp(currentDistance, closestDistance, originalDistance + minDistanceToObsticle);

        Vector3 newLocalPosition = Vector3.zero;
        switch (movementAxis)
        {
            case Helper.MovementAxis.x:
                newLocalPosition = Vector3.right * currentDistance;
                break;
            case Helper.MovementAxis.y:
                newLocalPosition = Vector3.up * currentDistance;
                break;
            case Helper.MovementAxis.z:
                newLocalPosition = Vector3.forward * currentDistance;
                break;
        }
        if (reverseMovementAxis)
        {
            newLocalPosition *= -1;
        }
        transform.localPosition = newLocalPosition + afterOffset;
    }


}
