﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndScreen : MonoBehaviour {

    [Tooltip("The text that will be updated at the end of each match")]
    public Text endText;

    private void Start()
    {
        Cursor.visible = true;

        if(LevelManager.winner == 3)
        {
            endText.text = "Draw!";
        }
        else
        {
            endText.text = "Team " + LevelManager.winner + " wins";
        }
    }

    public void backToMenu()
    {
        SceneManager.LoadScene(1);
        Prototype.NetworkLobby.LobbyManager lobbyManager = FindObjectOfType<Prototype.NetworkLobby.LobbyManager>();
        if(lobbyManager != null)
        {
            lobbyManager.EndGameLoadMainPanel();
        }
        
    }

}
