﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class Checkpoint : NetworkBehaviour {

    [SyncVar]
    float points = 50;
    short currentOwner = 0;

    public short Owner
    {
        get
        {
            if(points > 50)
            {
                return 1;
            }
            else if(points < 50)
            {
                return 2;
            }
            return 0;
        }
    }
    public float Points
    {
        get
        {
            return points;
        }
        set
        {
            if (isServer)
            {
                SetPoints(value);
            }            
        }
    }

    void SetPoints(float points)
    {
        if (points > 100)
        {
            Points = 100;
            return;
        }
        else if (points < 0)
        {
            Points = 0;
            return;
        }
        else
        {
            this.points = points;
        }
    }

    private void Start()
    {
        StartCoroutine(calcPoints());
    }

    private void OnTriggerEnter(Collider other)
    {
        PlayerInfo enteredPlayer = other.GetComponent<PlayerInfo>();
        if(enteredPlayer != null)
        {
            switch (enteredPlayer.teamID)
            {
                case 1:
                    currentOwner += 1;
                    break;
                case 2:
                    currentOwner -= 1;
                    break;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        PlayerInfo leavingPlayer = other.GetComponent<PlayerInfo>();
        if (leavingPlayer != null)
        {
            switch (leavingPlayer.teamID)
            {
                case 1:
                    currentOwner -= 1;
                    break;
                case 2:
                    currentOwner += 1;
                    break;
            }
        }
    }
   
    private IEnumerator calcPoints()
    {
        do
        {
            if(currentOwner > 0)
            {
                Points += 0.1f;
            }
            else if(currentOwner < 0)
            {
                Points -= 0.1f;
            }

            yield return new WaitForSeconds(0.1f);
        } while (true);
    }

    public Color GetCheckpointColor()
    {
        Color checkpointColor = new Color(1, 1, 1);
        float pointMultiplier = 0.25f + Mathf.Abs((points - 50)) / 100;

        if (points > 50)
        {
            checkpointColor = checkpointColor * (1 - pointMultiplier) + Settings.teamOneColor * pointMultiplier;
        }
        else if(points < 50)
        {
            checkpointColor = checkpointColor * (1 - pointMultiplier) + Settings.teamTwoColor * pointMultiplier;
        }

        return checkpointColor;
    }
}
