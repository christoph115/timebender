﻿using UnityEngine;
using Prototype.NetworkLobby;
using UnityEngine.Networking;

//Form Unity Example Network Lobby, added some code

public class TransferPlayerInfo : LobbyHook
{
    override
    public void OnLobbyServerSceneLoadedForPlayer(NetworkManager manager, GameObject lobbyPlayer, GameObject gamePlayer)
    {
        PlayerInfo playerInfo = gamePlayer.GetComponent<PlayerInfo>();
        playerInfo.playerName = lobbyPlayer.GetComponent<LobbyPlayer>().playerName;
        playerInfo.playerColor = lobbyPlayer.GetComponent<LobbyPlayer>().playerColor;
        playerInfo.teamID = lobbyPlayer.GetComponent<LobbyPlayer>().teamID;
        playerInfo.colorChanged = true;
    }
}
