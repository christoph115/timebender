﻿using UnityEngine;

public static class Settings {

    public static readonly float slowmotionMultiplier = 0.15f;
    public static readonly Color teamOneColor = Color.red;
    public static readonly Color teamTwoColor = Color.blue;

    public static float volume = 1;

}