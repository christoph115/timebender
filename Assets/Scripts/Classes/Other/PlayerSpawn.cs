﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerSpawn : NetworkBehaviour
{
    [Tooltip("Spawn points of team one")]
    public Transform[] teamOneSpawnPoints;
    [Tooltip("Spawn points of team two")]
    public Transform[] teamTwoSpawnPoints;

    private void Start()
    {
        StartCoroutine(spawnPlayer());
    }

    IEnumerator spawnPlayer()
    {
        yield return new WaitForSeconds(5);

        short teamOnePlayerNumber = 0;
        short teamTwoPlayerNumber = 0;
        Transform spawnPoint = null;

        foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        {
            switch (player.GetComponent<PlayerInfo>().teamID)
            {
                case 1:
                    if (teamOneSpawnPoints.Length <= teamOnePlayerNumber)
                    {
                        teamOnePlayerNumber = 0;
                    }
                    spawnPoint = teamOneSpawnPoints[teamOnePlayerNumber++];
                    break;

                case 2:
                    if (teamTwoSpawnPoints.Length <= teamTwoPlayerNumber)
                    {
                        teamTwoPlayerNumber = 0;
                    }
                    spawnPoint = teamTwoSpawnPoints[teamTwoPlayerNumber++];
                    break;
            }
            if (spawnPoint != null)
            {
                player.GetComponent<PlayerHealth>().spawnPosition = spawnPoint.position;
                player.transform.position = spawnPoint.position;
            }
        }
    }

}
