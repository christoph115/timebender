﻿using UnityEngine;

public class CameraScript : MonoBehaviour {

    public static Transform target;
    public FrostEffect slowmotionEffect;

    public void SetTarget(Transform newTarget)
    {
        target = newTarget;
        transform.position = target.position;
        transform.rotation = target.rotation;
        transform.SetParent(target);
    }

    internal void EnableSlowmotionEffect(bool enable)
    {
        slowmotionEffect.enabled = enable;
    }
}
