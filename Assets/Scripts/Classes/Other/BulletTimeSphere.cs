﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BulletTimeSphere : MonoBehaviour {

    [Tooltip("The lifetime of a timebubble")]
    public float lifetime = 10;
    [Tooltip("The time the bubble takes to grow")]
    public float upScaleTime = 3;
    [Tooltip("The time the bubble takes to shrink")]
    public float downScaleTime = 5;
    [Tooltip("The maximum size on the x-axis")]
    public float maxScaleX = 10;
    [Tooltip("The maximum size on the y-axis")]
    public float maxScaleY = 10;
    [Tooltip("The maximum size on the z-axis")]
    public float maxScaleZ = 10;

    bool upScaling = true;
    float scalingTime = 0;
    float startTime;
    float upScalePerSecondX;
    float upScalePerSecondY;
    float upScalePerSecondZ;

    float downScalePerSecondX;
    float downScalePerSecondY;
    float downScalePerSecondZ;

    List<SlowmotionObject> currentlySlow = new List<SlowmotionObject>();

    private void Start()
    {
        if (upScaleTime + downScaleTime > lifetime)
        {
            upScaleTime = lifetime - downScaleTime;
        }

        upScalePerSecondX = (maxScaleX - transform.localScale.x) / upScaleTime;
        upScalePerSecondY = (maxScaleY - transform.localScale.y) / upScaleTime;
        upScalePerSecondZ = (maxScaleZ - transform.localScale.z) / upScaleTime;

        downScalePerSecondX = (maxScaleX - transform.localScale.x) / downScaleTime;
        downScalePerSecondY = (maxScaleY - transform.localScale.y) / downScaleTime;
        downScalePerSecondZ = (maxScaleZ - transform.localScale.z) / downScaleTime;

        startTime = Time.time;
        Destroy(gameObject, lifetime);
    }

    private void OnTriggerEnter(Collider other)
    {
        SlowmotionObject slowmotionObject = other.gameObject.GetComponent<SlowmotionObject>();
        if (slowmotionObject != null)
        {
            slowmotionObject.toggleSlowmotion(Settings.slowmotionMultiplier, true);
            currentlySlow.Add(slowmotionObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        SlowmotionObject slowmotionObject = other.gameObject.GetComponent<SlowmotionObject>();
        if (slowmotionObject != null)
        {
            slowmotionObject.toggleSlowmotion(Settings.slowmotionMultiplier, false);
            currentlySlow.Remove(slowmotionObject);
        }
    }

    private void OnDestroy()
    {        
        foreach(SlowmotionObject slowmotionObject in currentlySlow)
        {
            if(slowmotionObject != null)
            {
                try
                {
                    slowmotionObject.toggleSlowmotion(Settings.slowmotionMultiplier, false);
                }
                catch(Exception e)
                {
                    Debug.Log(e);
                }                
            }            
        }
    }

    private void Update()
    {
        if (upScaling)
        {
            scalingTime += Time.deltaTime;
            float timeToDo = Time.deltaTime;

            if (scalingTime > upScaleTime)
            {
                timeToDo -= scalingTime - upScaleTime;
                scalingTime = downScaleTime;
                upScaling = false;
            }

            float newXScale = transform.localScale.x + upScalePerSecondX * timeToDo;
            float newYScale = transform.localScale.y + upScalePerSecondY * timeToDo;
            float newZScale = transform.localScale.z + upScalePerSecondZ * timeToDo;

            transform.localScale = new Vector3(newXScale, newYScale, newZScale);
        }
        else if (downScaleTime > 0 && lifetime - downScaleTime <= Time.time - startTime)
        {
            float timeToDo = Time.deltaTime;

            if (downScaleTime == scalingTime)
            {
                scalingTime -= (Time.time - startTime) - (lifetime - downScaleTime);
                timeToDo = downScaleTime - scalingTime;
            }
            else
            {
                scalingTime -= Time.deltaTime;
            }

            float newXScale = transform.localScale.x - downScalePerSecondX * timeToDo;
            float newYScale = transform.localScale.y - downScalePerSecondY * timeToDo;
            float newZScale = transform.localScale.z - downScalePerSecondZ * timeToDo;

            transform.localScale = new Vector3(newXScale, newYScale, newZScale);
        }
    }

}
